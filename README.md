# Probabilistic coherent gust model

This repository contains the script and data for the probabilistic coherent gust model from the paper: [Á. Hannesdóttir, D. R. Verelst, & A. M. Urbán. Extreme coherent gusts with direction change - probabilistic model, yaw control and wind turbine loads. *Wind Energy Science*, 8, 231-245, 2023](https://doi.org/10.5194/wes-8-231-2023).
The data contains estimated gust parameters from Høvsøre and the gust model is therfore site specific for Høvsøre.

## Cite this work

If you use this data or code for academic research, you are encouraged to cite the following paper:

```
@article{hannesdottir2022,
  title   = {Extreme coherent gusts with direction change - probabilistic model,
yaw control and wind turbine loads},
  author  = {Hannesd\'ottir, \'{A}sta and Verelst, David R. and Urb\'an, Albert M.},
  journal = {Wind Energy Science},
  volume  = {8},
  year    = {2023},
  number = {2},
  pages = {231--245},
  url = {https://wes.copernicus.org/articles/8/231/2023/},
  doi = {10.5194/wes-8-231-2023}
}
```
## Questions

To get help on how to use the data or code, you can contact the main author by email or open an issue in the GitLab "Issues" section.