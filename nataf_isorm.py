#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 10:19:30 2020

@author: astah
"""
#%%
import numpy as np
from scipy.special import gamma
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.stats import weibull_min, norm, gumbel_r, weibull_max, chi2, kstest
from mpl_toolkits.mplot3d import Axes3D
plt.rcParams["axes.grid"] = True
plt.close('all')

hovs_vars = np.load('steyn_par.npz')

#%% Statistics Høvsøre

du_hub = hovs_vars['steyn_par_hub'][:,1] - hovs_vars['steyn_par_hub'][:,0]
dt_hub = hovs_vars['steyn_par_hub'][:,3]*3.17
dt_top = hovs_vars['steyn_par_top'][:,3]*3.17
du_top = hovs_vars['steyn_par_top'][:,1] - hovs_vars['steyn_par_top'][:,0]
dt_bot = hovs_vars['steyn_par_bot'][:,3]*3.17
du_bot = hovs_vars['steyn_par_bot'][:,1] - hovs_vars['steyn_par_bot'][:,0]

dt = (dt_hub + dt_top + dt_bot)/3
dt[du_bot<=0] = (dt_hub[du_bot<=0] + dt_top[du_bot<=0])/2
du = (du_hub + du_top + du_bot)/3
du[du_bot<=0] = (du_hub[du_bot<=0] + du_top[du_bot<=0])/2

d_dir = hovs_vars['d_dir']

lag = hovs_vars['steyn_par_hub'][:,2] - hovs_vars['steyn_par_top'][:,2]
lag_bot = hovs_vars['steyn_par_bot'][:,2] - hovs_vars['steyn_par_hub'][:,2]

u_b = hovs_vars['steyn_par_hub'][:,0]
u_a = hovs_vars['steyn_par_hub'][:,1]
u_mean = (u_b + u_a)/2

#%% Fit distributions to the parameters

v_rated = 11.4

ind_rated = np.where( (u_b <= v_rated) & (u_a >= v_rated) )[0]

min_dir = 0

ind_ev = ind_rated[d_dir[ind_rated] > min_dir]

gumpar_du = gumbel_r.fit(du[ind_ev])
print(kstest(du[ind_ev], 'gumbel_r', args=gumpar_du))

# Fit the Weibull to the d_dir
weipar_dir = weibull_min.fit(d_dir[ind_ev],loc=0)
print(kstest(d_dir[ind_ev], 'weibull_min', args=weipar_dir))

# Fit the Weibull to the dt
weipar_dt = weibull_max.fit(-dt[ind_ev],floc=0)
print(kstest(-dt[ind_ev], 'weibull_max', args=weipar_dt))

# np.savetxt('gust_variables.txt', (du[ind_ev],d_dir[ind_ev],dt[ind_ev]))

n_bins = 60
fig, axarr = plt.subplots(1,3,figsize=(12*0.7,4*0.7))
#fig, axarr = plt.subplots(3,1,figsize=(5,10))
n, bins, patches = axarr[0].hist(du[ind_ev], n_bins, fc='k', lw=2, \
                        histtype='step', density=True, cumulative=True,\
                        label = 'Empirical CDF')
axarr[0].plot(bins,gumbel_r.cdf(bins,*gumpar_du),'k--', linewidth=1.5, \
     label='Fitted Gumbel')
axarr[0].autoscale(enable=True,axis='x',tight=True)
axarr[0].legend(loc='best')
axarr[0].set_xlabel(r'$\Delta$u [ms$^{-1}$]')
axarr[0].set_ylabel('CDF')
n, bins, patches = axarr[1].hist(d_dir[ind_ev], n_bins, fc='k', lw=2, \
                        histtype='step', density=True, cumulative=True,\
                        label = 'Empirical CDF')
axarr[1].plot(bins,weibull_min.cdf(bins,*weipar_dir),'k--', linewidth=1.5, \
     label='Fitted Weibull')
axarr[1].autoscale(enable=True,axis='x',tight=True)
axarr[1].legend(loc='best')
axarr[1].set_xlabel(r'$\Delta \theta$ [deg]')
axarr[1].set_ylabel('CDF')
n, bins, patches = axarr[2].hist(-dt[ind_ev], n_bins, fc='k', lw=2, \
                        histtype='step', density=True, cumulative=True,\
                        label = 'Empirical CDF')
axarr[2].plot(bins,weibull_max.cdf(bins,*weipar_dt),'k--', linewidth=1.5, \
     label='Fitted Weibull')
axarr[2].autoscale(enable=True,axis='x',tight=True)
axarr[2].legend(loc='best')
axarr[2].set_xlabel(r'$-\Delta t$ [s]')
axarr[2].set_ylabel('CDF')
plt.tight_layout()

n_bins = 20
fig, axarr = plt.subplots(1,3,figsize=(12*0.7,4*0.7))
n, bins, patches = axarr[0].hist(du[ind_ev], n_bins, fc='k', lw=2, \
                        histtype='step', density=True, cumulative=False,\
                        label = 'Empirical PDF')
axarr[0].plot(bins,gumbel_r.pdf(bins,*gumpar_du),'k--', linewidth=1.5, \
     label='Fitted Gumbel')
axarr[0].autoscale(enable=True,axis='x',tight=True)
axarr[0].legend(loc='best')
axarr[0].set_xlabel(r'$\Delta$u [m/s]')
axarr[0].set_ylabel('PDF')
n, bins, patches = axarr[1].hist(d_dir[ind_ev], n_bins, fc='k', lw=2, \
                        histtype='step', density=True, cumulative=False,\
                        label = 'Empirical PDF')
axarr[1].plot(bins,weibull_min.pdf(bins,*weipar_dir),'k--', linewidth=1.5, \
     label='Fitted Weibull')
axarr[1].autoscale(enable=True,axis='x',tight=True)
axarr[1].legend(loc='best')
axarr[1].set_xlabel(r'$\Delta \theta$ [deg]')
axarr[1].set_ylabel('PDF')
n, bins, patches = axarr[2].hist(-dt[ind_ev], n_bins, fc='k', lw=2, \
                        histtype='step', density=True, cumulative=False,\
                        label = 'Empirical PDF')
axarr[2].plot(bins,weibull_max.pdf(bins,*weipar_dt),'k--', linewidth=1.5, \
     label='Fitted Weibull')
axarr[2].autoscale(enable=True,axis='x',tight=True)
axarr[2].legend(loc='best')
axarr[2].set_xlabel(r'$-\Delta t$ [s]')
axarr[2].set_ylabel('PDF')
plt.tight_layout()

#%% Correlation matrices with Liu an Der Kiureghian corrections

def f_weibweib(rho,k0,shift0,k1,shift1):
    """ This formulae is from Liu and Der Kiureghian where both model distributions
    are Weibull. Delta is sigma/mu and estimated from the Weibull distribution.
    """
    delta0 = np.sqrt(gamma(1+2/k0) - gamma(1+1/k0)**2)/(gamma(1+1/k0) +shift0)
    delta1 = np.sqrt(gamma(1+2/k1)-gamma(1+1/k1)**2)/(gamma(1+1/k1) + shift1)
    F = 1.063 - 0.004*rho - 0.2*(delta0+delta1) - 0.001*rho**2 + 0.337*\
    (delta0**2+delta1**2) + 0.007*rho*(delta0+delta1) - 0.007*delta0*delta1
    return F

def f_gumbweib(rho,k,shift):
    """ This formulae is from Liu and Der Kiureghian where one model 
    distributions is Weibull and the other is Gumbel. Delta is sigma/mu and 
    estimated from the distributions.
    """
    delta = np.sqrt(gamma(1+2/k) - gamma(1+1/k)**2)/(gamma(1+1/k) +shift)
    F = 1.064 + 0.065*rho - 0.21*delta + 0.003*rho**2 + 0.356*delta**2 \
        - 0.211*rho*delta
    return F


rho01 = np.corrcoef([du[ind_ev],d_dir[ind_ev]])[0,1]
rho02 = np.corrcoef([du[ind_ev],-dt[ind_ev]])[0,1]
rho12 = np.corrcoef([d_dir[ind_ev],-dt[ind_ev]])[0,1]

rho01m = rho01*f_gumbweib(rho01,weipar_dir[0],weipar_dir[1])
rho02m = rho02*f_gumbweib(rho02,weipar_dt[0],0)
rho12m = rho12*f_weibweib(rho12,weipar_dir[0],weipar_dir[1],weipar_dt[0],0)

R01 =  [[ 1., rho01m],[rho01m, 1.]]
R02 =  [[ 1., rho02m],[rho02m, 1.]]
R12 =  [[ 1., rho12m],[rho12m, 1.]]

R = [[ 1., rho01m, rho02m],[rho01m, 1., rho12m],[rho02m, rho12m, 1.]]

#%% 2D Nataf transformation
T = 50

# IFORM:
bet = abs(norm.ppf(10.25/(T*len(ind_ev))))
# ISORM:
beta = np.sqrt(chi2.ppf(1- 10.25/(T*len(ind_ev)), df=2))

L2 = np.linalg.cholesky(R01)

phi = np.linspace(0, 2 * np.pi, 360)
u_0 = beta*np.cos(phi)
u_1 = beta*np.sin(phi)

u2 = np.stack((u_0,u_1))

z = np.dot(L2,u2)

x_1 = weibull_min.ppf( norm.cdf(z[1]), *weipar_dir )
x_0 = gumbel_r.ppf( norm.cdf(z[0]), *gumpar_du )

plt.figure()
plt.plot(x_0,x_1)
plt.scatter(du[ind_ev],d_dir[ind_ev])

#%% 3D Nataf transformation
beta = np.sqrt(chi2.ppf(1- 10.25/(T*len(ind_ev)), df=3))
nr1 = 400
v = np.linspace(0, 2 * np.pi, nr1)
w = np.linspace(0, np.pi, nr1)
u0 = beta * np.outer(np.cos(v), np.sin(w))
u1 = beta * np.outer(np.sin(v), np.sin(w))
u2 = beta * np.outer(np.ones(np.size(v)), np.cos(w))

u = np.stack((u0.flatten(),u1.flatten(),u2.flatten()))

L = np.linalg.cholesky(R)

z_sphere = np.dot(L,u)

x0 = gumbel_r.ppf( norm.cdf(z_sphere[0,:]), *gumpar_du )
x1 = weibull_min.ppf( norm.cdf(z_sphere[1,:]), *weipar_dir )
x2 = weibull_max.ppf( norm.cdf(z_sphere[2,:]), *weipar_dt )

# fig = plt.figure(figsize=(9,9))
# ax0 = fig.add_subplot(111, projection='3d')
# ax0.scatter(x0,x1,np.abs(x2),s=1)

#%%
fig = plt.figure(figsize=(9.5,4))
ax0 = fig.add_subplot(121, projection='3d')
ax0.scatter(du[ind_ev],d_dir[ind_ev],dt[ind_ev], color='C1')
ax0.plot_wireframe(x0.reshape(nr1,nr1),x1.reshape(nr1,nr1),
                   np.abs(x2.reshape(nr1,nr1)),linewidths=0.5)
ax0.set_xlabel(r'$\Delta$ u [ms$^{-1}$]',fontsize=10)
ax0.set_ylabel(r'$\Delta \theta$ [deg]',fontsize=10)
ax0.set_zlabel(r'$\Delta$t [s]',fontsize=10)
ax0.view_init(40, -120)
ax1 = fig.add_subplot(122, projection='3d')
ax1.scatter(du[ind_ev],d_dir[ind_ev],dt[ind_ev], color='C1')
ax1.plot_wireframe(x0.reshape(nr1,nr1),x1.reshape(nr1,nr1),
                   np.abs(x2.reshape(nr1,nr1)),linewidths=0.5)
ax1.set_xlabel(r'$\Delta$ u [ms$^{-1}$]',fontsize=10)
ax1.set_ylabel(r'$\Delta \theta$ [deg]',fontsize=10)
ax1.set_zlabel(r'$\Delta$t [s]',fontsize=10)
ax1.view_init(20, 160)
plt.tight_layout()

#%% Probability test of the IEC ECD

y0 = norm.ppf(gumbel_r.cdf( du[ind_ev],*gumpar_du))
y1 = norm.ppf(weibull_min.cdf( d_dir[ind_ev],*weipar_dir))
y2 = norm.ppf(weibull_max.cdf( -dt[ind_ev],*weipar_dt))

F0 = gumbel_r.cdf(15,*gumpar_du)
F1 = weibull_min.cdf(72,*weipar_dir) 
F2 = weibull_max.cdf(-10,*weipar_dt)

Z = norm.ppf(F0), norm.ppf(F1), norm.ppf(F2)

U = np.dot(np.linalg.inv(L),Z)
beta_prob = np.sqrt(U[0]**2 + U[1]**2 + U[2]**2)

P_event = 1 - chi2.cdf(beta_prob**2, df=3)
T_event = 10.25/(P_event*len(ind_ev))
print('T: ', T_event)
